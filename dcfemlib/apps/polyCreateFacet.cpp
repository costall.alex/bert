// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <domain3d.h>
#include <domain2d.h>
using namespace DCFEMLib;
using namespace MyMesh;

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

using namespace std;


int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-m] polygon-filename");
  lOpt.setDescription( (string) "Creates a 3D facet poly from 2D poly or 2D mesh" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "marker", required_argument, 'm', "int: Boundary marker for this facet [1]" );
  lOpt.insert( "output", required_argument, 'o', "string: outfilename" );

  bool help = false, verbose = false;
  int marker = 1;
  int option_char = 0, option_index = 0, tracedepth = 0;
  string outfileName = NOT_DEFINED;
    while ( ( option_char = getopt_long( argc, argv, "?hv"
				                    "o:"
                                                    "m:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'm': marker=atoi( optarg ); break;
    case 'o': outfileName = optarg; break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  string domainName( argv[argc-1] );
  

    Domain3D output;

    if ( verbose ) cout << "Load: " << domainName << endl;
    
    if ( domainName.find( ".bms" ) != string::npos ){
        if ( outfileName == NOT_DEFINED ){
            outfileName = domainName.substr( 0, domainName.rfind( ".bms" ) ) + ".poly";
        }
        Mesh2D mesh2d( domainName );
        if ( verbose ) mesh2d.showInfos();
        output.merge( mesh2d, marker );
    } else {
        if ( outfileName == NOT_DEFINED ){
            outfileName = domainName.substr( 0, domainName.rfind( ".poly" ) ) + ".poly" ;
        }
        Domain2D poly( domainName );
        output.createFacet( poly, marker );
    }

    if ( verbose ) cout << "Save: " << outfileName << endl;
    output.save( outfileName );
    return EXIT_SUCCESS;
}
