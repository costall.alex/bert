// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef MESH3D__H
#define MESH3D__H MESH3D__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "basemesh.h"
#include "mesh2d.h"
using namespace MyMesh;

namespace MyMesh{

class DLLEXPORT Mesh3D : public BaseMesh{
public:
  Mesh3D() : BaseMesh(){}
  Mesh3D( const Mesh3D & mesh );
  Mesh3D( const string & filename );
//   Mesh3D & operator = ( const Mesh3D & mesh );
//   Mesh3D & operator = ( const BaseMesh & mesh );

  virtual ~Mesh3D();
  
  //*! Retruns the dimension if the mesh. For this kind of meshs, the function allways returns 3,
  int dim() const { return 3; }
  //  virtual Mesh3D & operator = (const Mesh3D & mesh){ TO_IMPL  }
  //  virtual void clear();

  virtual void showInfos(){ 
    cout << "Nodes: " << pNodeVector_.size() 
	 << "\tTetrahedrons: " << pCellVector_.size() 
	 << "\tTriangles: " << pBoundaryVector_.size() << endl;
  }

  inline BaseElement * createTetrahedron( Node & a, Node & b, Node & c, Node & d, int id = -1, double attribute = 1.0 ){
    return createTetrahedron_( a, b, c, d, id, attribute );
  } 
  BaseElement * createTetrahedron( vector < Node * > & pNodeVector, int id = -1, double attribute = 1.0 );

  inline BaseElement * createHexahedron( Node & n1, Node & n2, Node & n3, Node & n4, 
					 Node & n5, Node & n6, Node & n7, Node & n8, int id = -1, double attribute = 1.0 ){
    return createHexahedron_( n1, n2, n3, n4, n5, n6, n7, n8, id, attribute );
  } 

  inline BaseElement * createTriangleFace( Node & a, Node & b, Node & c, int id = -1, int marker = 0 ){
    return createTriangleFace_( a, b, c, id, marker );
  }
  BaseElement * createTriangleFace( vector < Node * > & pNodeVector, int id = -1, int marker = 0 );

  inline BaseElement * createQuadrangleFace( Node & a, Node & b, Node & c, Node & d, int id = -1, int marker = 0 ){
    return createQuadrangleFace_( a, b, c, d, id, marker );
  }

  BaseElement * createCellFromNodeIdxVector( const vector < long > & idx, double attribute );
  BaseElement * createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker );

  virtual void createNeighoursInfos();

  virtual int load( const string & fbody, const string & style, IOFormat format = Ascii );
  virtual int load( const string & fbody, IOFormat format = Ascii ){ return load( fbody, "DCFEMLib", format ); }

  //  int loadMyFEMLib( const string & fname );
  int loadGRUMPMesh( const string & fname );
  int loadUserGRUMPMesh( const string & fbody );
  int loadTETGENMesh( const string & fbody, bool withFace = false );
  int loadTetGenMesh( const string & fbody, bool withFace = false,  bool tet10node = false );
  int loadTetGenNodes( const string & fbody );
  int loadFEMLABMesh( const string & fbody );
  int loadNetGenMesh( const string & fbody );

  int loadGetfemMesh( const string & fname );

  int saveOOGL_Off( const string & fname, double shrink = 1.0);
  int saveFacesGrumpBdry( const string & fname);
  int saveFacesOOGL_OffFile( const string & fname );
  int saveINRIAMeshFile( const string & fbody, RVector & datas = (*new RVector( 0 )), bool logScaleData = false);
  //  int saveVTKUnstructured( const string & fbody, RVector & data = (*new RVector( 0 )), bool logScaleData = false );
  int saveGMVFile( const string & fbody );


  //  void createN2Mesh3D( const Mesh3D & mesh );
  virtual void createH2Mesh( const BaseMesh & mesh );
  virtual void createP2Mesh( const BaseMesh & mesh );

  virtual int refine(){ TO_IMPL return -1;}
  virtual int createRefined( const BaseMesh & mesh, const vector < int > & cellIdx ){ TO_IMPL return -1; }

  virtual BaseElement * findBoundary( Node & n0, Node & n1, Node & n2 );
  virtual BaseElement * findBoundary( BaseElement & n0, BaseElement & n1 );
//   virtual BaseElement & findBoundary( const BaseElement & n0, const BaseElement & n1 ) const {
//     TO_IMPL;
//     return BaseElement;
//   }


  BaseElement & face( int i ) const { return boundary( i ); }
  BaseElement & face( int i ) { return boundary( i ); }

  int faceCount() const { return boundaryCount(); }

  void convertFromMesh2D( const Mesh2D & mesh );
  
  //  BaseMesh findSubMeshByAttribute( double from, double to = -1 );

  BaseElement * findCellAt( RealPos & pos );
  BaseElement * findCellAt( TriangleFace & face );

protected:
  BaseElement * createTetrahedron_( Node & a, Node & b, Node & c, Node & d, int id, double attribute );
  BaseElement * createTetrahedron10_( vector < Node * > & pNodeVector, int id, double attribute );

  BaseElement * createHexahedron_( Node & n1, Node & n2, Node & n3, Node & n4,
				   Node & n5, Node & n6, Node & n7, Node & n8, int id, double attribute );

  BaseElement * createTriangleFace_( Node & a, Node & b, Node & c, int id, int marker );
  BaseElement * createTriangle6Face_( vector < Node * > & pNodeVector, int id, int marker );
  BaseElement * createQuadrangleFace_( Node & a, Node & b, Node & c, Node & d, int id, int marker );

};

} // MyMesh

#endif // MESH3D__H

/*
$Log: mesh3d.h,v $
Revision 1.13  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.12  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.11  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.10  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.9  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.8  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.7  2005/05/03 18:46:47  carsten
*** empty log message ***

Revision 1.6  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.5  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.4  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.3  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.2  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
