HowTo: Include a reservoir into a model 

Authors: Thomas G�nther (LIAG) and Thomas Agricola (Uni Frankfurt)

Task: Create a 3d mesh that incorporates both topography and reservoir
        as an additional region in the mesh for inversion tasks
Prerequisites: Digital elevation model (DEM), dam geometry, water level
Files:  topo.xyz(DEM), thomas-z0.dat(DAT), mklines.m, mkmesh/merge.sh
Problem: Combine the two very complicated geometries by creating faces

The work is originated from a large-scale resistivity measurement done
in a valley whose river was dammed by a reservoir.
An extended water body in the model is definitely affecting resistivity
measurements and should be included as body with known geometry and
resistivity. The elevation model is available from the time where the dam
was constructed.
First we build an inversion mesh using a 3D DEM, and then we will mainly
add two additional faces:
1. The water horizontal surface between stream and dam (outline)
2. The vertical dam itself (dam)
These form a closed polygon that needs to be included as in the surface
mesh as with the BERT key TOPOPOLY. See G�nther et al. (2006) and Updhuay
et al. (2011) for a synthetic and a real example, respectively.

################################################################
Part I - Preparation of the outline of the two faces with matlab
We load the topographic model and make a meshgrid of it
Then we create a 2d polygon of the dam line
    dam=[49 213;-300 512]; %dam line points and end points
    dam(1,:)=dam(1,:)+diff(dam)*0.032;
    dam(2,:)=dam(2,:)+diff(dam)*0.062;
    xi=linspace(dam(1,1),dam(2,1),30);
    yi=linspace(dam(1,2),dam(2,2),30);
interpolate the elevation on it
    zi=interp2(X,Y,Z,xi,yi);zi([1 end])
and round it to 0.1 accuracy before saving it to a file
    xyzi=round([xi(:) yi(:) zi(:)]*10)/10;
??  xyzi=flipud(xyzi);
    save foot.xyz xyzi -ascii

For the outline of the water level 164.5m we determine the intersection
points of the level with each topography line and add the dam ends
    um=[];zw=164.5;
    for i=1:length(x),
       fi=find(diff(sign(Z(:,i)-zw)));
       for l=1:length(fi),
          yy=y(fi(l):fi(l)+1);
          w1=abs((Z(fi(l),i)-zw)/diff(Z(fi(l):fi(l)+1,i)));
          yyy=[1-w1 w1]*y(fi(l):fi(l)+1);
          if (x(i)>-320)&&(yyy<1200)&&(yyy>222), um=[um;x(i) yyy]; end
       end
    end
    um=[um;xyzi([1 end],1:2)];
    um(:,3)=zw;
    um=round(um*10)/10;
After removing bad points the file is saved into outline.xyz
and the two files are pasted together to the close polygon poly.xyz
As a result we have the files foot/outline/poly.xyz and foot2d.xz

#########################################
Part II - Preparation of the surface mesh

We could either create a cfg file with the keys
    TOPOPOINTS=topo.xyz
    TOPOPOLY=poly.xyz
or call the BERT1 executables directly:

    createSurface -v -a topo.xyz -P poly.xyz -b 30 -p 7 -q 30 -m 0.0 \
                  -o mesh/mesh thomas-z0.dat
    closeSurface -v -z 500 -a 0 -o mesh/mesh mesh/meshFine.poly  

createSurface triangulates an inner box in an outer box with electrodes
and polygones using triangle and interpolates the elevation on it.
The switches -b/-p/-q denote outer/inner boundary(in %) and mesh quality.
closeSurface closes the surface mesh by closing an inner and outer box
and we obtain the mesh PLC mesh/mesh.poly which we need in part IV.

##################################################
Part III - Create surface meshes for the two faces

First we go for the outline. We take the x/y values and create a 2d mesh
using polyCreateWorld, mesh it with dctriangle, create a 3D PLC (z=0)
out of it with polyCreateFacet and shift it to the target height.

    polyCreateWorld -d2 -C -t outline.xyz outline #->outline.poly
    dctriangle -q 30 outline #->outline.bms
    polyCreateFacet -v -o outline3d outline.bms #->outline3d.poly
    polyTranslate -z $ALTITUDE outline3d

The case is similar for the dam mesh (we use the before created 2d xz
file instead of xyz). We start with creating the 3d poly.
    polyCreateWorld -d2 -C -t foot2d.xz foot2d #->foot2d.poly
We bring first point to (0,0) for later rotation
    polyTranslate -y -$ALTITUDE foot2d
    dctriangle -q 30 foot2d #->foot2d.bms
    polyCreateFacet -v -o foot3d foot2d.bms
Now we first rotate the mesh to the x-z plane to be vertical
    polyRotate -x 90 foot3d
and finally rotate the mesh into the right position

X1=`head -1 foot.xyz|awk '{print $1}'`
Y1=`head -1 foot.xyz|awk '{print $2}'`
X2=`tail -1 foot.xyz|awk '{print $1}'`
Y2=`tail -1 foot.xyz|awk '{print $2}'`
ANGLE=`echo $X1 $X2 $Y1 $Y2|awk '{printf("%.12f",atan2($4-$3,$2-$1)*90/atan2(1,0))}'`     ??? head and tail -n ==> awk atan2 ==> angle
    polyRotate -z $ANGLE foot3d

The resulting PLC files can be converted into VTK using polyConvert -V
and visualized in Paraview to see whether they are matching.
    
####################################################
Part IV - Merging the individual PLC and create mesh

We finally merge the three PLC into a single on using polyMerge:
    polyMerge -t $TOLERANCE outline3d foot3d reservoir3d
    polyMerge -t $TOLERANCE mesh/mesh reservoir3d all

The -t switch defines a snap tolerance and should be very coarse (few dm).
Note, that the order of merging (second PLC into first) can be important.
The consistency of the individual PLC can be checked using
    tetgen -d $PLC
    meshconvert -v -it -o 
    
    
    tetgen -pazVAC -q1.3 wirklichalles
    meshconvert -v -iT -BDVM -o wirklichalles wirklichalles.1
    

References:
G�nther,R�cker&Spitzer (2006): 3-D modeling and inversion of DC resistivity data
incorporating topography - Part II: Inversion. - Geophys. J. Int. 166, 506-517.
Updhuay et al.(2011): Three-dimensional resistivity tomography in extreme
coastal terrain amidst dense cultural signals: application to cliff stability 
assessment at the historic D-Day site. - Geophys. J. Int. 173, xxx-xxx.
