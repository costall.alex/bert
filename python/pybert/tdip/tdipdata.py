import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import pygimli as pg
import pybert as pb
from . mipmodelling import DCIPMModelling


from pybert.importer import importAsciiColumns, importRes2dInv


def importTDIPdata(filename, verbose=False):
    """Read in TDIP data."""
    header = {}
    if isinstance(filename, str):
        ext = filename[filename.rfind('.')+1:]
        if ext.lower() in ['txt', 'tx2', 'gdd']:
            data, header = importAsciiColumns(filename, verbose=verbose,
                                              return_header=True)
        elif ext.lower() in ['dat', 'res2dinv']:
            data, header = importRes2dInv(filename, verbose=verbose,
                                          return_header=True)
        else:
            data = pb.importData(filename)
    elif isinstance(filename, pg.DataContainer):
        data = filename
    else:
        raise TypeError("Cannot use this type:"+type(filename))

    ipkey = ''
    testkeys = ['IP_#{}(mV/V)', 'M{}', 'ip{}']
    for key in testkeys:
        if data.exists(key.format(1)):
            ipkey = key
    MA = []
    i = 1
    while data.exists(ipkey.format(i)):
        ma = data(ipkey.format(i))
        if max(ma) <= 0:
            break

        MA.append(ma)
        i += 1

    MA = np.array(MA)
    t = np.arange(MA.shape[0]) + 1  # default: t=gate no
    if 'ipGateT' in header:
        t = header['ipGateT'][:-1] + np.diff(header['ipGateT'])/2
    testkeys = ['TM{}']
    for key in testkeys:
        if data.exists(key.format(1)):
            tt = [data(key.format(i+1))[0] for i in range(
                MA.shape[0])]
            t = np.array(tt)
            if sum(t) > 100:
                t *= 0.001

    return data, MA, t, header


class TDIPdata():
    """Class managing time-domain induced polarisation (TDIP) field data."""

    def __init__(self, filename=None, **kwargs):
        """Constructor with optional data load.

        Parameters
        ----------
        filename : str
            name of file to read in, allowed formats are:
                * Syscal Pro export file (*.txt)
                * ABEM TXT export file (*.txt) or raw time series)
                * Syscal Pro binary file (*.bin)
                * Aarhus Workbench processed data (*.tx2)
                * res2dinv data

        **kwargs:
            * paraGeometry : PLC (pygimli mesh holding geometry)
                plc for the 2d inversion domain
            * paraMesh : pygimli Mesh instance
                plc for the 2d inversion domain
            * verbose : bool
                Be verbose.
        """
        self.verbose = kwargs.get('verbose', False)
        self.basename = 'base'  # for saving results and images
        self.figs = {}  # figure container
        self.header = {}  # header for supplemental information
        self.t = kwargs.pop('t', [])  # time vector
        self.data = kwargs.pop('data', None)  # data container
        self.rhoa = kwargs.pop('rhoa', None)  # app. resistivity matrix [Ohm m]
        self.MA = kwargs.pop('ma', None)  # app. phases matrix [Grad, deg]
        self.ERT = None  # Resistivity manager class instance
        self.sINV = None  # single inversion instance
        self.pd = None  # paraDomain
        self.res = None  # resistivity
        self.m = None  # single-time spectral chargeability
        self.customParaGeometry = kwargs.pop('paraGeometry', None)
        self.customParaMesh = kwargs.pop('paraMesh', None)

        if filename is not None:
            self.load(filename, **kwargs)
            self.data.set('k', pg.geometricFactors(self.data, dim=2))

    def __repr__(self):  # for print function
        """String representation of the class."""
        out = ['TDIP data: ' + self.data.__str__() + ' nt=' + str(len(self.t))]
        if hasattr(self, 'header'):
            for key in self.header:
                val = self.header[key]
                if isinstance(val, str):
                    out.append(val)
                elif isinstance(val, int) or isinstance(val, float):
                    out.append(key+' = '+str(val))
                else:
                    out.append(key+' = array('+str(val.shape)+')')
        return "\n".join(out)

    def load(self, filename, **kwargs):
        """Try loading all supported file types:

            ABEM LS text output
            Syscal Pro text output
            Syscal Pro binary file
            res2dinv format
        """
        self.data, self.MA, self.t, self.header = importTDIPdata(filename)
        self.data.checkDataValidity(remove=False)
        print(type(self.MA), np.shape(self.MA))
        self.MA = self.MA[:, np.array(self.data('valid'), dtype=bool)]
        print("after:", self.data, self.MA.shape)
        self.data.removeInvalid()
        self.ensureRhoa()
        if isinstance(filename, str):
            self.basename = filename[:filename.rfind('.')]

        return

    def ensureRhoa(self):
        """Make sure apparent resistivity is present in file."""
        if not self.data.allNonZero('k'):
            self.data.set('k', pb.geometricFactors(self.data, 2))  # check dim
        if not self.data.allNonZero('rhoa'):
            if not self.data.allNonZero('r'):
                self.data.set('r', self.data('u')/self.data('i'))

            self.data.set('rhoa', self.data('r') * self.data('k'))

    def filter(self, tmin=0, tmax=1e9, kmax=1e6, electrode=None, forward=False,
               a=None, b=None, m=None, n=None, ab=None, mn=None, corrSID=1,
               rmin=0, rmax=1e9, nr=[]):
        """Filter data with respect to frequencies and geometric factor.

        Parameters
        ----------

        tmin : double
            minimum frequency
        tmax : double
            maximum frequency
        kmax : double
            maximum (absolute) geometric factor
        electrode : int
            electrode to be removed completely
        a/b/m/n : int
            delete data with specific current or potential electrode
        ab/mn : int
            delete data with specific current or potential dipole lengths
        corrSID: int [1]
            correct sensor index (like in data files)
        """
        print("filtering: nd={:d}, nt={:d}".format(*self.MA.shape))

        ind = (self.t >= tmin) & (self.t <= tmax)
        self.MA = self.MA[ind, :]
        self.t = self.t[ind]
        ind = (np.abs(self.data('k')) <= kmax)  # maximum geometric factor
        ind[self.data('rhoa') < rmin] = False
        ind[self.data('rhoa') > rmax] = False
        ind[nr] = False  # individual numbers
        am = self.data("m") - self.data("a")
        if ab is not None:
            ind[np.isclose(np.abs(self.data("b")-self.data("a")), ab)] = False
        if mn is not None:
            ind[np.isclose(np.abs(self.data("n")-self.data("m")), mn)] = False
        if forward:
            ind[am < 0] = False  # reverse measurements
            print(sum(ind))
        for name in ['a', 'b', 'm', 'n']:
            u = list(np.atleast_1d(eval(name)))
            if electrode is not None:
                u.extend(list(np.atleast_1d(electrode)))
            for uu in u:
                ind = ind & np.not_equal(self.data(name) + corrSID, uu)

        self.data.set('valid', pg.RVector(self.data.size()))
        self.data.markValid(pg.find(ind))
        self.data.removeInvalid()
        self.MA = self.MA[:, ind]

        if electrode is not None:
            self.data.removeUnusedSensors()
        print("filtered: nd={:d}, nf={:d}".format(*self.MA.shape))

    def showRhoa(self, **kwargs):
        """Show apparent resistivity."""
        kwargs.setdefault('cMap', 'Spectral_r')
        kwargs.setdefault('logScale', True)
        return pb.show(self.data, **kwargs)

    def setGates(self, t=None, dt=None, delay=0.0):
        """Set gate midpoints (t) or lengths (dt) and delay."""
        if t is None:
            assert dt is not None, "gate length and delay time must be set"
            self.dt = np.array(dt)
            t = np.cumsum(self.dt) - self.dt/2 + delay
            self.header['ipGateT'] = np.cumsum(np.hstack((0, dt))) + delay
            self.header['dt'] = dt
            self.header['delay'] = delay

        assert len(t) == self.MA.shape[0]
        self.t = t

    def integralChargeability(self, normalize=True, **kwargs):
        """Compute integral chargeability by summing up windows x dt.

        Parameters
        ----------
            normalize : bool [True]
                normalize such that mV/V is retrieved, otherwise msec
            start : int [0]
                first gate to take
            stop : int [self.MA.shape[1]]
                last gate to take
        Returns
        -------
            integral chargeability : numpy.array

        """
        start = kwargs.pop('start', 1)
        stop = kwargs.pop('stop', len(self.MA))
        mint = pb.pg.RVector(self.data.size())
        for i in range(start-1, stop):
            mint += self.MA[i] * self.t[i]

        if normalize:
            mint /= sum(self.t[start-1:stop])

        self.data.set('mint', mint)

        return np.array(self.data('mint'))

    def showIntegralChargeability(self, **kwargs):
        """Show integral chargeability (kwargs forwarded to pb.show)."""
        if not self.data.haveData('mint'):
            self.data.set('mint', self.integralChargeability(**kwargs))
        kwargs.setdefault('cMap', 'plasma')
        kwargs.setdefault('logScale', True)
        return pb.show(self.data, self.data('mint'), **kwargs)

    def showMa(self, nr=0, **kwargs):
        """Show apparent chargeability (kwargs forwarded to pb.show)."""
        kwargs.setdefault('cMap', 'plasma')
        kwargs.setdefault('logScale', True)
        return pb.show(self.data, self.MA[nr], **kwargs)

    def fitDecays(self, show=True):
        """Fit decays by exponential function."""
        G = np.ones((len(self.t), 2))
        G[:, 1] = self.t
        Ginv = np.linalg.inv(G.T.dot(G)).dot(G.T)
        ma = np.abs(self.MA)
        ab = Ginv.dot(np.log(ma))
        self.data.set('M0', np.exp(ab[0]))
        self.data.set('tau', -1./ab[1])
        if show:
            pb.show(self.data, 'M0', label='M0 [mV/V]')
            pb.show(self.data, 'tau', label=r'$\tau$ [s]')

    def generateDataPDF(self, rdict=None, mdict=None, **kwargs):
        """Generate a multi-page pdf file with all data as pseudosections.

        Parameters
        ----------
        rdict : dict
            dictionary with plotting arguments for apparent resistivity
        """
        if rdict is None:
            rdict = dict(logScale=True, cMap='Spectral_r',
                         label=r'$\rho_a$ [$\Omega$m]', xlabel='x [m]')
        if mdict is None:
            mdict = dict(cMin=1, cMax=np.max(self.MA), logScale=True,
                         cMap='plasma', label=r'$m_a$ [mV/V]', xlabel='x [m]')
        mdict.update(**kwargs)
        rdict.setdefault('cMap', 'Spectral_r')  # default color scale
        mdict.setdefault('cMap', 'plasma')
        fig, ax = plt.subplots()
        with PdfPages(self.basename+'-alldata.pdf') as pdf:
            pb.show(self.data, 'rhoa', ax=ax, **rdict)
            ax.set_title('apparent resistivity')
            fig.savefig(pdf, format='pdf')
            if self.data.allNonZero('err'):
                fig.clf()
                ax = fig.add_subplot(111)
                pb.show(self.data, self.data('err')*100+0.01, ax=ax,
                        label=r'$\epsilon$ [%]')
                ax.set_title('error')
                fig.savefig(pdf, format='pdf')
            if self.data.allNonZero('i'):
                fig.clf()
                ax = fig.add_subplot(111)
                pb.show(self.data, self.data('i')*1000, ax=ax,
                        label=r'$I$ [mA]')
                ax.set_title('current')
                fig.savefig(pdf, format='pdf')
            if self.data.allNonZero('u'):
                fig.clf()
                ax = fig.add_subplot(111)
                pb.show(self.data, self.data('u')*1000, ax=ax,
                        label=r'$U$ [mV]')
                ax.set_title('voltage')
                fig.savefig(pdf, format='pdf')
            if self.data.allNonZero('tau'):
                fig.clf()
                ax = fig.add_subplot(111)
                tau = np.abs(self.data('tau').array())
                cMin, cMax = np.nanquantile(tau, [0.03, 0.97])
                pb.show(self.data, tau, ax=ax, label=r'$\tau$ [s]',
                        ind=(tau > 0), logScale=True, cMin=cMin, cMax=cMax)
                ax.set_title('apparent relaxation time')
                fig.savefig(pdf, format='pdf')
            if self.data.allNonZero('M0'):
                fig.clf()
                ax = fig.add_subplot(111)
                pb.show(self.data, 'M0', ax=ax, **mdict)
                ax.set_title('fitted (t=0) chargeability')
                fig.savefig(pdf, format='pdf')
            for i, ma in enumerate(self.MA):
                fig.clf()
                ax = fig.add_subplot(111)
                pb.show(self.data, ma, ax=ax, **mdict)
                tstr = ''
                if 'ipGateT' in self.header:
                    tstr = ' (t={:g}-{:g}s)'.format(
                            *(self.header['ipGateT'][i:i+2]))
                ax.set_title('apparent chargeability gate ' + str(i+1) + tstr)
                fig.savefig(pdf, format='pdf')

    def generatePDF(self, **kwargs):
        """Backward compatibility."""
        return self.generateDataPDF(**kwargs)

    def showDecay(self, nr=[], ax=None, ab=None, mn=None, verbose=True,
                  **kwargs):
        """Show decay curves for groups of data.

        Parameters
        ----------
        nr : iterable
            list of data indices to show, if not given ab/mn are analysed
        ab : [int, int]
            list of sensor numbers for current injection (counting from 1)
        mn : [int, int]
            list of sensor numbers for potential (counting from 1)

        Plotting Keywords
        -----------------
        label : str
            legend label for single data, otherwise generated from A,B,M,N
        basename : str
            string to prepend to automatically generated label
        marker : str ['x']
            marker to use for plotting
        xlim, ylim : [float, float]
            limits for x or y axis
        xlog : str ['linear']
            scaling of x axis
        ylog, ylog : str ['log']
            scaling of y axis
        """
        data = self.data
        bs = kwargs.pop('basename', 'abmn')
        if ab is not None:
            a = np.minimum(data('a'), data('b'))
            b = np.maximum(data('a'), data('b'))
            # nr.extend(pg.find((a == min(ab)-1) & (b == max(ab)-1)))
            nr = np.nonzero(np.isclose(a, min(ab)-1) &
                            np.isclose(b, max(ab)-1))[0]
        if mn is not None:
            m = np.minimum(data('m'), data('n'))
            n = np.maximum(data('m'), data('n'))
            # fi = pg.find((m == min(mn)-1) & (n == max(mn)-1))
            fi = np.nonzero(np.isclose(m, min(mn)-1) &
                            np.isclose(n, max(mn)-1))[0]
            if ab is not None:  # already chose AB dipole => select
                nr = np.intersect1d(nr, fi)
            else:
                nr.extend(fi)
        if verbose:
            print("nr=", nr)
        kwargs.setdefault('marker', 'x')
        if isinstance(nr, int):
            nr = [nr]
        if len(nr) > 0:
            if ax is None:
                fig, ax = plt.subplots()
            for nn in nr:
                abmn = [int(self.data(t)[nn]+1) for t in ['a', 'b', 'm', 'n']]
                kw = dict(kwargs)
                kw.setdefault('label', (bs+': '+'{:d} '*4).format(*abmn))
                ax.semilogy(self.t, self.MA[:, nn], **kw)

            ax.grid(True)
            ax.legend()
            if 'xlim' in kwargs:
                ax.set_xlim(kwargs['xlim'])
            if 'ylim' in kwargs:
                ax.set_xlim(kwargs['ylim'])
            if 'xlog' in kwargs:
                ax.set_xscale(kwargs['xlog'])
            if 'ylog' in kwargs:
                ax.set_yscale(kwargs['ylog'])
            return ax

    def generateDecayPDF(self, **kwargs):
        """Generate a pdf file with all decays sorted by current injections."""
        from matplotlib.backends.backend_pdf import PdfPages

        ab = self.data('a') * 1000 + self.data('b')
        uab = np.array(np.unique(ab), dtype=int)
        # sort them according AB length (major) and A electrode (minor)
        dip = np.abs(uab // 1000 - (uab % 1000)) * 1000 + uab // 1000
        uab = uab[np.argsort(dip)]
        with PdfPages(self.basename + '-decays.pdf') as pdf:
            fig, ax = plt.subplots()
            for u in uab:
                ab = [u // 1000 + 1, (u % 1000) + 1]
                ax.cla()
                self.showDecay(ab=ab, ax=ax, **kwargs)
                fig.savefig(pdf, format='pdf')

    def save(self, filename=None):
        """Save all data in some (yet-to-be-defined or -decided) format."""
        pass

    def invertRhoa(self, **kwargs):
        """Invert apparent resistivity. kwargs forwarded to ERTManager."""
        if self.ERT is None:
            self.ERT = pb.ERTManager()

        self.ERT.setData(self.data)
        show = kwargs.pop('show', False)
        if 'mesh' in kwargs:  # more a hack until invert accepts mesh again
            self.ERT.setMesh(kwargs.pop('mesh'))
        self.res = self.ERT.invert(**kwargs)
        self.coverage = self.ERT.coverageDC()
        if show:
            self.showResistivity()

    def invertMa(self, nr=0, ma=None, **kwargs):
        """Invert for chargeability."""
        if ma is None:
            if nr == -1 or nr == '0':
                ma = self.data('M0') * 0.001
            else:
                ma = self.MA[nr] * 0.001  # should MA be in V/V instead?

        maerr = np.ones_like(ma) * kwargs.pop('error', 0.002)
        maerr[ma < 0] = 1e5
        maerr[ma > 1] = 1e5
        ma[ma < 0] = 0.001
        ma[ma > 1] = 0.99
        fIP = DCIPMModelling(self.ERT.fop, self.ERT.mesh, self.ERT.resistivity)
        show = kwargs.pop('show', False)
        if 'regionFile' in kwargs:
            fIP.regionManager().loadMap(kwargs.pop('regionFile'))
        else:
            if self.ERT.fop.regionManager().regionCount() > 1:
                fIP.region(1).setBackground(True)
                fIP.region(2).setConstraintType(1)

        fIP.regionManager().setZWeight(kwargs.pop('zWeight', 1.0))
        fIP.createRefinedForwardMesh(True)
        tD, tM = pg.RTransLog(), pg.RTransLogLU(0, 0.99)
        INV = pg.RInversion(ma, fIP, tD, tM, True, False)
        mstart = pg.RVector(len(self.ERT.resistivity), pg.median(ma))
        INV.setModel(mstart)
        INV.setAbsoluteError(maerr)
        INV.setLambda(kwargs.pop('lam', 100))
        INV.setRobustData(kwargs.pop('robustData', False))
        INV.setBlockyModel(kwargs.pop('blockyModel', False))
        self.m = INV.run()
        if show:
            self.showChargeability()

    def showResistivity(self, ax=None, **kwargs):
        """Show resistivity inversion result."""
        kwargs.setdefault('label', r'resistivity [$\Omega$m]')
        kwargs.setdefault('cMap', 'Spectral_r')
        return pg.show(self.ERT.paraDomain, self.res, ax=ax, **kwargs)

    def showChargeability(self, ax=None, **kwargs):
        """Show chargeability inversion result."""
        kwargs.setdefault('label', 'chargeability [mV/V]')
        kwargs.setdefault('cMap', 'plasma')
        return pg.show(self.ERT.paraDomain, self.m*1e3, ax=ax, **kwargs)

    def showResults(self, rkw={}, mkw={}):
        """Show result (s)."""
        fig, ax = plt.subplots(nrows=2)
        self.showResistivity(ax=ax[0])
        self.showChargeability(ax=ax[1])

    def individualInversion(self, **kwargs):
        """Carry out individual inversion for spectral chargeability."""
        self.invertRhoa(**kwargs)
        self.M = np.zeros((len(self.MA), len(self.res)))
        for i, ma in enumerate(self.MA):
            print('Inverting gate {}'.format(i))
            self.invertMa(ma, **kwargs)
            self.M[i] = self.m


if __name__ == "__main__":
    pass
#    from glob import glob
#    filenames = glob('example*.dat')+glob('example*.txt')+glob('example*.tx2')
#    tdip = TDIPdata()  # filenames[2])
#    self = tdip
#    tdip.load(filenames[2])
#    tdip.showRhoa()
#    tdip.showMa(10)
#    print(tdip)
