// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef ELEMENTS__H
#define ELEMENTS__H ELEMENTS__H

#include "dcfemlib.h"
using namespace DCFEMLib;

// #include "log.h"
// #include "numfunct.h"
// #include "errors.h"
// #include "global.h"
#include "stlmatrix.h"

#include "myvec/vector.h"
#include <fstream>
#include <vector>
#include <set>

using namespace std;

namespace MyMesh{

template <class T> class Pos;
class MeshEntity;
class Node;
class RegionMarker;
class BaseElement;
class Boundary;
class Cell;
class Edge;
class Edge3;
class Triangle;
class Triangle6;
class TriangleFace;
class Triangle6Face;
class Rectangle;
class Quadrangle;
class QuadrangleFace;
class Tetrahedron;
class Tetrahedron10;
class Hexahedron;

typedef std::vector< Node * > VectorpNodes;
typedef std::vector< Edge * > VectorpEdges;
typedef std::vector< BaseElement * > VectorpElements;
typedef std::vector< Boundary * > VectorpBoundaries;
typedef std::vector< Cell * > VectorpCells;
typedef std::vector< TriangleFace * > VectorpFaces;
typedef std::vector< RegionMarker * > VectorpRegionMarker;
typedef std::set< Node * > SetpNodes;
typedef std::set< Edge * > SetpEdges;
typedef std::set< Triangle * > SetpTriangles;
typedef std::set< TriangleFace * > SetpTriangleFaces;
typedef std::set< Tetrahedron * > SetpTetrahedrons;
typedef std::set< BaseElement * > SetpBaseElements;
typedef std::set< BaseElement * > SetpCells;
typedef std::set< BaseElement * > SetpBounds;

#define MYMESH_NODE_RTTI 10000010
#define MYMESH_BASEELEMENT_RTTI 10002000
#define MYMESH_EDGE_RTTI 100020002
#define MYMESH_EDGE3_RTTI 100020012
#define MYMESH_TRIANGLE_RTTI 100020003
#define MYMESH_TRIANGLE6_RTTI 100020013
#define MYMESH_QUADRANGLE_RTTI 100020004
#define MYMESH_QUADRANGLEFACE_RTTI 100020104
#define MYMESH_TRIANGLEFACE_RTTI 100020101
#define MYMESH_TRIANGLE6FACE_RTTI 100020102
#define MYMESH_REGIONMARKER_RTTI 100020005
#define MYMESH_TETRAHEDRON_RTTI 100030001
#define MYMESH_TETRAHEDRON10_RTTI 100030002
#define MYMESH_HEXAHEDRON_RTTI 100030003

enum TriangleStatus{NOTREFINED, REGULAR, IRREGULAR};
enum AngleType{RAD,DEG};

typedef Pos< int > IntPos;
typedef Pos< double > RealPos;

DLLEXPORT ostream & operator << ( ostream & str, const Node & n );

void show ( const SetpCells & a );
void show ( const SetpTriangleFaces & a );
DLLEXPORT RealPos averagePosition( const VectorpNodes & nodesVector );
DLLEXPORT RealPos averagePosition( const vector < RealPos > & posVector );
double jacobianDeterminantWithoutZ( const RealPos & p0, const RealPos & p1, const RealPos & p2);
DLLEXPORT int loadPosList( const string & filename, vector < RealPos > & posVector );

void swap( vector < RealPos > & electrodeList );

template < class T > void translate( vector < Pos<T> > & vec, const Pos<T> & tr ){
    for ( size_t i = 0; i < vec.size(); i ++ ) vec[ i ].translate( tr.x(), tr.y(), tr.z() );
}

vector < RealPos > operator - (const vector < RealPos > & a, const vector < RealPos > & b );
vector < RealPos > operator + (const vector < RealPos > & a, const vector < RealPos > & b );
vector < RealPos > operator * (const vector < RealPos > & a, const double v );
double operator * ( const std::valarray < double > & a, const RealPos & b );

DLLEXPORT bool xVari( const vector < RealPos > & electrodeList );
DLLEXPORT bool yVari( const vector < RealPos > & electrodeList );
DLLEXPORT bool zVari( const vector < RealPos > & electrodeList );

DLLEXPORT bool posX_less( const RealPos & a, const RealPos & b );
DLLEXPORT bool posX_more( const RealPos & a, const RealPos & b );
DLLEXPORT bool pPosX_less( RealPos * a, RealPos * b );

//! A X-Y-Z Position.
template < class T > class DLLEXPORT Pos {
public:
  // For convenience we define a more meaningful name for the Template type
  typedef T ScalarType;

  /*! Default constructor. Constructs a Pos at (0,0,0) */
  Pos() { x_ = 0; y_ = 0; z_ = 0; valid_ = false; }
  Pos( T x, T y ) : x_( x ), y_( y ){ z_ = 0; valid_ = true;  }
  Pos( T x, T y, T z ) : x_( x ), y_( y ), z_( z ) { valid_ = true;}
  Pos( const MyVec::STLVector & vec ) : x_( vec[0] ), y_( vec[1] ), z_( vec[2] ) { valid_ = true;}
  /*! Default destructor. Destructs the position. */

//   Pos( int size ) { // size allways 3, hack for template functions
//     x_ = 0; y_ = 0; z_ = 0; valid_ = true; }

  ~Pos( ){}

  /*! Copyconstructor */
  Pos( const Pos< T > & p ) : x_( p.x() ), y_( p.y() ), z_( p.z() ), valid_( p.valid() ) {
  }

  Pos( const RealPos & p, const MyVec::STLMatrix & wm ){
    RealPos tmp( p );
    tmp.transform( wm );
    x_ = (int)rint( tmp.x() );
    y_ = (int)rint( tmp.y() );
    z_ = (int)rint( tmp.z() );
    valid_ = true;
  }

  //  Pos( const RealPos & p, const MyVec::STLMatrix & wm );

  Pos< T > & operator = ( const Pos< T > & p ){
    if ( this != & p ){
      x_ = p.x();
      y_ = p.y();
      z_ = p.z();
      valid_ = p.valid();
    }
    return *this;
  }
  Pos< T > & operator = ( const MyVec::STLVector & vec ){
    x_ = vec[ 0 ];
    y_ = vec[ 1 ];
    z_ = vec[ 2 ];
    valid_ = true;
    return *this;
  }
  Pos< T > & operator -= ( const Pos< T > & p ){
    x_ -= p.x();
    y_ -= p.y();
    z_ -= p.z();
    valid_ = true;
    return *this;
  }
  Pos< T > & operator += ( const Pos< T > & p ){
    x_ += p.x();
    y_ += p.y();
    z_ += p.z();
    valid_ = true;
    return *this;
  }
  Pos< T > & operator *= ( const T & val ){
    x_ *= val;
    y_ *= val;
    z_ *= val;
    valid_ = true;
    return *this;
  }
  Pos< T > & operator /= ( const T & val ){
    x_ /= val;
    y_ /= val;
    z_ /= val;
    valid_ = true;
    return *this;
  }

  T x( ) const { return x_;}
  T x( ) { return x_;}
  T y( ) const { return y_;}
  T y( ) { return y_;}
  T z( ) const { return z_;}
  T z( ) { return z_;}

  T operator [] ( int i ) const {
    if ( i == 0 ) return x_;
    if ( i == 1 ) return y_;
    return z_;
  }

  T & operator [] ( int i ){
    if ( i == 0 ) return x_;
    if ( i == 1 ) return y_;
    return z_;
  }

  static int dim() { return 3; }
  int size() const { return dim(); }

  void setX( const T x ) { x_ = x; valid_ = true; }
  void setY( const T y ) { y_ = y; valid_ = true; }
  void setZ( const T z ) { z_ = z; valid_ = true; }

  void setXY( const T x, const T y) { x_ = x; y_ = y; valid_ = true; }
  void setXYZ( const T x, const T y, const T z) { x_ = x; y_ = y; z_ = z; valid_ = true; }

  double abs() const { return sqrt( this->scalar(*this) ); }
  double distance( const RealPos & p ) const { return ( (*this) - p ).abs(); }
  double distance( const RealPos & p ) { return ( (*this) - p ).abs(); }

  //  double angleCos( const Pos< T > & p ) const ;
  double angle( const Pos< T > & p, AngleType aType = RAD ) const ;
  double angle( const Pos< T > & p1, const Pos< T > & p3, AngleType aType = RAD ) const ;

  Pos< T > normOf( const Pos< T > & p1, const Pos< T > & p2 ) const { return norm( p1, p2 ); }
  Pos< T > norm( const Pos< T > & p1, const Pos< T > & p2 ) const;
  DLLEXPORT Pos< T > norm( const Pos< T > & p1 ) const;

  void normalise( const Pos< T > & p1 ){ this /= this->abs(); }
  Pos< T > normalisedCopy( ) const { Pos < T > tmp( *this ); return tmp/tmp.abs(); }

  Pos< T > crossProduct( const Pos< T > & p ) const ;
  Pos< T > cross( const Pos< T > & p ) const { return crossProduct( p ); }

  double scalar( const RealPos & p ) const {
    return ( x() * p.x() + y() * p.y() + z() * p.z() );
  }
  double scalarProduct( const Pos< T > & p )  const { return scalar( p ); }
  double dot( const RealPos & p ) const { return this->scalar( p ); }

  Pos< T > middlePosition( const Pos< T > & p ) const { return ( (*this) + p ) / 2.0; }

  bool linearlyDependend( const RealPos & p );
  bool orthogonal( const RealPos & p );

  bool isClockwise( const RealPos & p1, const RealPos & p3 ) const ;
  double jacobianDeterminant( const Pos< T > & p1, const Pos< T > & p3 ) const ;

  template <class Matrix > void transform( const Matrix & wm ){
    double x = x_, y = y_, z = z_;
//         std::cout << "RP->Tr-" << wm << std::endl;
//     for ( uint i = 0; i < 4; i ++ ){
//             for ( uint j = 0; j < 4; j ++ ){
//                     std::cout << wm[i ][j] << " ";
//             }
//             std::cout << std::endl;
//         }
    x_ = x * wm[0][0] + y * wm[0][1] + z * wm[0][2] + 1.0 * wm[0][3];
    y_ = x * wm[1][0] + y * wm[1][1] + z * wm[1][2] + 1.0 * wm[1][3];
    z_ = x * wm[2][0] + y * wm[2][1] + z * wm[2][2] + 1.0 * wm[2][3];
  }

  void translate( double tx, double ty, double tz );
  void translate( const RealPos & pos ){ translate(pos.x(), pos.y(), pos.z() ); }
  void rotateX( double phi );
  void rotateY( double phi );
  void rotateZ( double phi );
  void rotate( double phiX, double phiY, double phiZ );
  void rotate( const RealPos & rot ){ rotate( rot[ 0 ], rot[ 1 ], rot[ 2 ] ); }

  RealPos round( double tol = TOLERANCE ) const {
    return RealPos( floor( x_ / tol + 0.5 ) * tol, floor( y_ / tol + 0.5 ) * tol , floor( z_ / tol + 0.5) * tol );
  }

  bool valid() const { return valid_; }
  void setValid( bool valid ) { valid_ = valid ; }

  //  bool isWithin( BaseElement & base, double tolerance = TOLERANCE, bool verbose = false ) const;
  bool isWithin( const Edge & edge, double tolerance = TOLERANCE, bool verbose = false ) const;
  bool isWithin( const Triangle & tri, double tolerance = TOLERANCE, bool verbose = false ) const;
  bool isWithin( const Tetrahedron & tet, double tolerance = TOLERANCE, bool verbose = false ) const;

  void randomize( ){
 TO_IMPL
//    setXYZ( 1.0f*random()/RAND_MAX,
//	    1.0f*random()/RAND_MAX,
//	    1.0f*random()/RAND_MAX );
  }

  MyVec::STLVector vec( ) const {
    MyVec::STLVector vec(3);
    vec[ 0 ] = x_; vec[ 1 ] = y_; vec[ 2 ] = z_;
    return vec;
  }

protected:
  T x_, y_, z_;
  bool valid_;
};

template < class T > ostream & operator << ( ostream & str, const Pos< T > & p ){
  if ( p.valid() ){
    str << "(" << p.x() << ", " << p.y() << ", " << p.z() << ") ";
  } else {
    str << " Position: invalid ";
  }
  return str;
}
template < class T > ofstream & operator << ( ofstream & str, const Pos< T > & p ){
  if ( p.valid() ){
    str << p.x() << "\t" << p.y() << "\t" << p.z();
  } else {
    str << " Position: invalid ";
  }
  return str;
}

// template < class T > T operator * ( const Pos< T > & p1, const Pos< T > & p2 ){
//   cout << WHERE_AM_I << " WArning use scalar();" << endl;
//   //  return ( p1.x() * p2.x() ) + ( p1.y() * p2.y() ) + ( p1.z() * p2.z() );
//     return -1;
// }
template < class T > Pos< T > operator * ( const Pos< T > & p0, const Pos< T > & p1);
template < class T > Pos< T > operator * ( const Pos< T > & p0, const Pos< T > & p1){
  Pos< T > tmp( p0 );
  tmp.setX( tmp.x() * p1.x() );
  tmp.setY( tmp.y() * p1.y() );
  tmp.setZ( tmp.z() * p1.z() );
  return tmp;
}
template < class T > Pos< T > operator / ( const Pos< T > & p0, const Pos< T > & p1);
template < class T > Pos< T > operator / ( const Pos< T > & p0, const Pos< T > & p1){
  Pos< T > tmp( p0 );
  tmp.setX( tmp.x() / p1.x() );
  tmp.setY( tmp.y() / p1.y() );
  tmp.setZ( tmp.z() / p1.z() );
  return tmp;
}
template < class T > Pos< T > operator * ( const Pos< T > & p, const T & val){
  Pos< T > tmp( p );
  return tmp *= val;
}
template < class T > Pos< T > operator / ( const Pos< T > & p, const T & val ){
  Pos< T > tmp( p );
  return tmp /= val;
}
template < class T > Pos< T > operator - ( const Pos< T > & p1, const Pos< T > & p2 ){
  Pos< T > tmp( p1 );
  return tmp -= p2;
}
template < class T > Pos< T > operator + ( const Pos< T > & p1, const Pos< T > & p2 ){
  Pos< T > tmp( p1 );
  return tmp += p2;
}

//! An MeshEntity :)
/*! An MeshEntity which holds an id */
class MeshEntity{
public:
  /*! Set the node id */
  inline void setId( int id ) { _id = id; }
  /*! Returns the node id */
  inline int id() const { return _id; }
  /*! Returns the node id */
  inline int id() { return _id; }

  /*! rtti for ObjectTyp identifcation: \n
    MYMESH_NODE_RTTI, MYMESH_REGIONMARKER,
    MYMESH_BASEELEMENT_RTTI, MYMESH_EDGE_RTTI, MYMESH_EDGE3_RTTI, MYMESH_TRIANGLE_RTTI, MYMESH_TRIANGLE6_RTTI,
    MYMESH_QUADRANGLE_RTTI, MYMESH_TRIANGLEFACE_RTTI, MYMESH_TRIANGLE6FACE_RTTI, MYMESH_REGIONMARKER_RTTI,
    MYMESH_TETRAHEDRON_RTTI, MYMESH_TETRAHEDRON10_RTTI */
  virtual int rtti() const = 0;

protected:
  int _id;
};

//! A Node.
/*! A Node with an id, a Position double x, and double z and two lists: one with all Edges which came from the node and one with all Edges which go to this Nodes. */
class DLLEXPORT  Node : public MeshEntity{
public:
  /*! Create empty Node */
  Node(){ _pos.setValid( false ); }
//   /*! Create a Node at Position x,z */
//   Node(double x, double z, int id = -1, int marker = 0);
  /*! Create a Node at Position x,z */
  Node(double x, double y, double z, int id = -1, int marker = 0);
  /*! Create a Node at Real Position pos */
  Node( const RealPos & pos, int id = -1, int marker = 0);
  /*! Create a copy of Node */

  Node( const Node & node);
  /*! Default destructor */
  virtual ~Node();

  virtual int rtti() const { return MYMESH_NODE_RTTI; }
  /*! Assigns a shallow copy of node to this Node and returns a reference to this Node */
  Node & operator = ( const Node & node );

  /*! Set the node x-position.*/
  inline void setX(double x){ _pos.setX( x ); }
  /*! Set the node y-position.*/
  inline void setY(double y){ _pos.setY( y ); }
  /*! Set the node z-position.*/
  inline void setZ(double z){ _pos.setZ( z ); }
  /*! Set the node x- and z-position.*/
  inline void setXY(double x, double y){ _pos.setX( x ); _pos.setY( y ); }
  /*! Set the node x-,y- and z-position.*/
  inline void setXYZ(double x, double y, double z){ _pos.setX( x ); _pos.setY( y ); _pos.setZ( z ); }
  /*! Set the Position.*/
  inline void setPos( const RealPos & pos ){ _pos = pos; }
  /*! Returns x-position from node.*/
  inline double x() const { return _pos.x(); }
  /*! Returns y-position from node.*/
  inline double y() const { return _pos.y(); }
  /*! Returns z-position from node.*/
  inline double z() const { return _pos.z(); }
  /*! Returns position from node.*/
  inline RealPos pos() const { return _pos; }
  /*! Returns position from node.*/
  inline RealPos & pos() { return _pos; }

  /*! Add \ref edge edge to List of 'fromEdges' */
  inline void addFromEdge( Edge & edge ) { _fromlist.insert( & edge ); }
  /*! Add \ref edge edge to List of 'toEdges' */
  inline void addToEdge( Edge & edge ) { _tolist.insert( & edge ); }

  /*! delets Edge edge from Set of 'fromEdges' */
  void eraseFromEdge( Edge & edge ){ _fromlist.erase( & edge ); }
  /*! delets Edge edge from Set fo 'toEdges' */
  void eraseToEdge( Edge & edge ){ _tolist.erase( & edge ); }

  /*! Returns the reference to common Edge between this and node. NULL if no common edge. */
  Edge * findEdge( Node & node );
  /*! Returns the angle between n1 -> this <- n2 .*/
  double angle( Node & n1, Node & n3 );
  /*! Returns the distance from this to node. */
  double distance( const Node & node );
  double distanceTo( const Node & node );
  //  double distanceTo( Node & node );
  /*! Smooth the mesh during moving the node in the middle of all neighbours. The smoothing function depend on function \n
   0 Laplace (geom Weightpoint) \n
   1 mod. Laplace (with weighting) */
  int smooth(int function);

  /*! Returns the middle Position between this and node */
  RealPos middlePosition( const Node & node ){ return _pos.middlePosition( node.pos() ); }
  /*! Returns the set of edges with this node ist startpoint */
  inline SetpEdges fromSet() const { return _fromlist; }
  /*! Returns the set of edges with this node ist endpoint */
  inline SetpEdges toSet() const { return _tolist; }

  /*! Collect all neighbour Elements an returns a set of all pointers */
  SetpBaseElements neighbourElements();

  /*! Collect all neighbour Nodes, without this node and returns a set of all pointers */
  SetpNodes neighbourNodes();

  /*! Returns the Nodemarker */
  inline int marker() const { return marker_; }
  /*! Sets the Nodemarker (source / target etc.)*/
  virtual void setMarker(int marker) { marker_ = marker; }

//   SetpCells faceSet() const { return pFaceSet_; }
//   void insertFace( BaseElement * face ){ pFaceSet_.insert( face ); }

  SetpCells cellSet(){ return pCellSet_; }
  void insertCell( BaseElement * cell ){ pCellSet_.insert( cell ); }

  SetpCells boundarySet() { return pFaceSet_; }
  void insertBoundary( BaseElement * face ){ pFaceSet_.insert( face ); }

protected:
  RealPos _pos;
  int marker_;
  SetpEdges _fromlist;
  SetpEdges _tolist;
  //  SetpTriangleFaces pFaceSet_;
  SetpBounds pFaceSet_;
  SetpCells pCellSet_;
};

class RegionMarker : public Node{
public:
  RegionMarker( const RegionMarker & marker )
    : Node( marker.pos() ), _dx ( marker.dx() ){
    _attributesVector.push_back( marker.attribute() );
  }
  RegionMarker( const RealPos & pos, double attribute = 1., double dx = 0.0 )
    : Node( pos ), _dx( dx )  { _attributesVector.push_back( attribute ); }
  RegionMarker( double x, double y, double attribute = 1., double dx = 0.0 )
    : Node( x, y, 0 ), _dx( dx ) { _attributesVector.push_back( attribute ); }
  virtual ~RegionMarker(){}

  virtual int rtti() const { return MYMESH_REGIONMARKER_RTTI; }
  double dx() const { return _dx; }
  void setDx( double dx ){ _dx = dx; }

  double area() const { return _dx; }
  void setArea( double area ){ _dx = area; }
  /*! Gets the i-th attribute of an element ( the first attribute is default -- attribute[0] ) */
  virtual double attribute( int pos = 0 ) const { return _attributesVector[ pos ]; /* Q&D at() waere besser geht aber nicht :( */}
  /*! Sets the i-th attribute of an element ( the first attribute is default -- attribute[0] )*/
  virtual void setAttribute( double attribute, int pos = 0 ) { _attributesVector[ pos ] = attribute; /* Q&D at() waere besser geht aber nicht :( */}

  /*! Returns an vector of all given Attributes */
  vector< double > & allAttributes() { return _attributesVector; }
  /*! sets the vector of all Attributes */
  void setAllAttributes( vector< double > attributesVector ) { _attributesVector = attributesVector; }

protected:
  vector< double > _attributesVector;
  double _dx;
};

//!A BaseElement
/*! Baseclass for FEM-Elements, holds an id, rtti, and a vector of double attributes for each element*/
class BaseElement : public MeshEntity{
public:
  /*! Constructs an empty Baseelement. */
  BaseElement();
  /*! BaseElement Destructor. */
  virtual ~BaseElement();

  virtual int rtti() const { return MYMESH_BASEELEMENT_RTTI; }
  /*! Gets the i-th attribute of an element ( the first attribute is default -- attribute[0] ) */
  virtual double attribute(int pos = 0) const { return _attributesVector[ pos ]; /* Q&D at() waere besser geht aber nicht :( */}
  /*! Sets the i-th attribute of an element ( the first attribute is default -- attribute[0] )*/
  virtual void setAttribute(double attribute, int pos = 0) { _attributesVector[ pos ] = attribute; /* Q&D at() waere besser geht aber nicht :( */}

  RealPos middlePos() const { return averagePosition( _nodesVector ); }

  /*! Returns an vector of all given Attributes */
  vector< double > allAttributes() const { return _attributesVector; }
  /*! sets the vector of all Attributes */
  void setAllAttributes( vector< double > attributesVector ) { _attributesVector = attributesVector; }

  /*! Returns a Reference to \ref Node No. i ( 0..size ) */
  virtual Node & node( int i );
  virtual Node & node( int i ) const;

  /*! Sets the Reference to NPointer node to Node No. i ( 0..size ) */
  virtual void setNode( int i, Node & node );

  int marker() const { return marker_; }
  int & marker() { return marker_; }
  void setMarker( int marker ) { marker_ = marker; }

  int nodeCount() const { return _nodesVector.size(); }
  virtual int shapeNodeCount() const = 0;

  //  virtual double jacobianDet( ) const { TO_IMPL return -1; }
  //  virtual double jacobianDeterminant( Node * n0 = NULL, Node * n1 = NULL, Node * n2 = NULL ) const { TO_IMPL return -1; }
  virtual double jacobianDeterminant( ) const {FUTILE return -1.0;}

  virtual MyVec::STLMatrix jacobianMat( ) const { FUTILE return MyVec::STLMatrix(); }

  virtual double partDerivationRealToUnity( char absPos, int relPos ){FUTILE return -1.0;}
  virtual double area() const = 0; //{ FUTILE return -1.0; }
  virtual RealPos normVector() const;
  virtual RealPos averagePos() const;

  TriangleStatus refineStatus(){ return _status; }
  void setRefineStatus( TriangleStatus status ){ _status = status; }

  virtual void check(){ cerr << WHERE_AM_I << "Hier sollte ich nicht sein" << endl; }
  //  virtual MeshEntity * touch( const RealPos & pos, double tol ) const ={ TO_IMPL return 0; }

  BaseElement * findBoundaryCell( );

  vector< Node * > nodesVector() { return _nodesVector; }

  virtual bool touch( const RealPos & pos, double tol, bool verbose = false ) const {
  	TO_IMPL
        return false;
    }

  /*! Returns a Pointer to the left Element */
  inline BaseElement * leftCell() const { return _el; }

  /*! Returns a Pointer to the right Element */
  inline BaseElement * rightCell() const { return _er; }

  /*! Sets the Pointer to the left Element */
  inline void setLeftCell( BaseElement & tri ){ _el = & tri; }

  /*! Sets the Pointer to the right Element */
  inline void setRightCell( BaseElement & tri ){ _er = & tri; }

protected:
  int checkEdge3( int a, int b, int c );

  vector< double > _attributesVector;
  vector< Node * > _nodesVector;
  TriangleStatus _status;
  int marker_;
  BaseElement * _el;
  BaseElement * _er;

};

// class Boundary{
// public:
//   Boundary(){};
//   ~Boundary(){};

//   /*! Returns the Edgemarker */
//   inline int marker() const { return marker_; }

//   /*! Returns reference to Edgemarker */
//   inline int & marker() { return marker_; }

//   /*! Sets the Edgemarker */
//   inline void setMarker(int marker) { marker_ = marker; }

// protected:

//   int marker_;
// };

ofstream & operator << ( ofstream & str, const BaseElement & p );
ofstream & operator << ( ofstream & str, BaseElement & p );

// class Cell{
// public:
//   Cell(){}
//   ~Cell(){}

//   /*! Returns the Edgemarker */
//   inline int marker() const { return marker_; }

//   /*! Returns reference to Edgemarker */
//   inline int & marker() { return marker_; }

//   /*! Sets the Edgemarker */
//   inline void setMarker(int marker) { marker_ = marker; }

// protected:
//   int marker_;
// };


//! An Edge.
/*! An Edge with an id, a startpoint \ref Node *a, and an endpoint \ref Node *b,
  a boundary- or materialinterface-marker and if exist a left and right \ref BaseElement.*/
class Edge : public BaseElement{
public:
  Edge(){ DO_NOT_USE }
  /*! Constructs an Edge with Node a and Node b, Marker ist set to 0*/
  Edge( Node & a, Node & b, int id = -1, int marker = 0 );
  /*! Create a copy of Edge */
  Edge( const Edge & edge );
  /*! Default destructor */
  virtual ~Edge();
  virtual int rtti() const { return MYMESH_EDGE_RTTI; };
  /*! Assigns a shallow copy of edge to this Edge and returns a reference to this Edge */
  Edge & operator = ( const Edge & edge );

//   Node & node( size_t nr ){
//     assert(nr > -1 && nr < _nodesVector.size() ); return *_nodesVector[ nr ]; }
//   void setNode( size_t nr, Node & node){
//     assert(nr > -1 && nr < _nodesVector.size() ); _nodesVector[ nr ] = & node; }
  virtual double area() const { return length(); }

  virtual int shapeNodeCount() const { return 2; }
  /*! Sets the Edges 'A' \ref Node */
  void setNodeA(Node & a);
  /*! Sets the Edges 'B' \ref Node */
  void setNodeB(Node & b);
  /*! Sets NodeA and NodeB */
  void setNodes(Node & a, Node & b);
  /*! Gets the Edges 'A' \ref Node */
  inline Node & nodeA() const { return * _nodesVector[0]; }
  /*! Gets the Edges 'B' \ref Node */
  inline Node & nodeB() const { return * _nodesVector[1]; }

  inline Node * pNodeA() { return  _nodesVector[ 0 ]; }
  inline Node * pNodeB() { return  _nodesVector[ 1 ]; }

  //  double at( double t ) const { return Line( _nodesVector[ 0 ].pos(), _nodesVector[ 1 ].pos() ).at( t ); ]
  /*! Returns the edge length. */
  double length() const;
  /*! Returns the intersection Position from this and edge. NULL if no intersection */
  RealPos intersectionPos( Edge & edge );

  virtual double partDerivationRealToUnity( char absPos, int relPos );

  RealPos normVector() const;

  /*! Returns the rise of this edge. ( function(x) = offset() + rise() * x. ) */
  double rise();
  /*! Returns the offset of this edge. ( function(x) = offset() + rise() * x. ) */
  double offset();

  /*! Swap Edge between neighbour Elements and only defined if both neighbours are Triangles. */
  int swap( int babuskacheck = 0 );
  /*! Erases Element dependency to edge, element should be deleted */
  void eraseElement( BaseElement & ele );
  int touch( const RealPos & pos ) const;

  virtual double jacobianDeterminant( ) const { return 0.0; };
protected:
};

ofstream & operator << ( ofstream & str, Edge & p );
ofstream & operator << ( ofstream & str, const Edge & p );
ostream & operator << ( ostream & str, Edge & p );
ostream & operator << ( ostream & str, const Edge & p );

class Edge3 : public Edge{
public:
  Edge3( Node & a, Node & b, Node & c, int id = -1, int marker = 0 );
  virtual ~Edge3();
  virtual int rtti() const { return MYMESH_EDGE3_RTTI; };
protected:
};

ofstream & operator << ( ofstream & str, const Edge3 & p );

//!A triangle.
/*! A triangle with an id, three nodes and one vector of attributes. Inherits some methods. (calculate Area, find elements) */
class Triangle : public BaseElement{
public:
  /*! Constructs an empty Triangle should not be used. */
  Triangle(){ cerr << __ASSERT_FUNCTION << " should not be used." << endl; }
  /*! Constructs an Triangle with 3 edges */
  Triangle( Node & n1, Node & n2, Node & n3, int id = -1, double attribute = 1.0, bool doit = true );
  /*! Create a copy of Triangle */
  Triangle( const Triangle & triangle );
  /*! Default destructor */
  virtual ~Triangle();
  virtual int rtti() const { return MYMESH_TRIANGLE_RTTI; };
  /*! Assigns a shallow copy of triangle to this Triangle and returns a reference to this Triangle */
  Triangle & operator = ( const Triangle & triangle );

  virtual int shapeNodeCount() const { return 3; }
  /*! Sets the Pointers nodes to Nodes 1 upto 3, Edges will set automaticaly */
  virtual void setNodes(Node & n1, Node & n2, Node & n3);
  //  virtual void setNode(int i, Node & node){ assert( i > -1 && i < 4 ); _nodesVector[i] = & node; }
  //  virtual Node & node(int i) { assert( i > -1 && i < 4 ); return *_nodesVector[i]; }
  /*! Returns the reference to this node in the triangle which is in opposite Position of edge */
  Node & oppositeTo( Edge & edge );

  /*! Returns a Reference to \ref Edge No. i (0..2) */
  Edge & edge(int i) const ;

  /*! Collect all three Edges with common Nodes n1, n2, n3. The tree nodes must be kind of one triangle. Nessesary to build a complete triangle without the knowledge of the edges. */
  VectorpEdges collectEdges( Node & n1, Node & n2, Node & n3 );

  /*! Calulates and returns the Jacobian determinant of \ref Triangle triangle, which
    corresponds the double-size of triangles Area. If this value is negative then this
    triangle has the wrong direction.
    (May the Jacobian-determinant be with you, and allways positive. :) )*/
  virtual double jacobianDeterminant( Node * n0, Node * n1, Node * n2 ) const;

  virtual MyVec::STLMatrix jacobianMat( ) const;
  virtual double jacobianDeterminant( ) const {
    return jacobianDeterminant( _nodesVector[0], _nodesVector[1], _nodesVector[2] ) ;
  }

  virtual double partDerivationRealToUnity( char absPos, int relPos );
  virtual double area() const { return jacobianDeterminant() / 2 ; }
  //  int nodeCount(){ return _nodesVector.size(); }

  RealPos middlePosition(){ return averagePosition( _nodesVector ); }
  void setIrregEdge( Edge & edge ){ _irregedge = & edge; }
  Edge & irregEdge(){ return *_irregedge; }

protected:
  Edge * _edges[3];
  Edge * _irregedge;
};

class Triangle6 : public Triangle{
public:
  Triangle6( Node & n1, Node & n2, Node & n3, int id = -1, double attribute = 1.0 );
  ~Triangle6();
  virtual int rtti() const { return MYMESH_TRIANGLE6_RTTI; };

  void check();
  void addNodeFromEdge3( Edge3 & edge );
protected:
};


class TriangleFace : public Triangle {
public:
  TriangleFace( Node & n1, Node & n2, Node & n3, int id = -1, int marker = -1 );
  virtual ~TriangleFace();
  virtual int rtti() const { return MYMESH_TRIANGLEFACE_RTTI; };
  virtual double jacobianDeterminant() const;
  virtual double area() const;
  virtual RealPos normVector() const ;

  void setLeftNeighbour( Tetrahedron * left ){ _leftNeighbour = left; }
  void setRightNeighbour( Tetrahedron * right ){ _rightNeighbour = right; }
  Tetrahedron * leftNeighbour(){ return _leftNeighbour;}
  Tetrahedron * rightNeighbour(){ return _rightNeighbour;}

protected:
  Tetrahedron * _leftNeighbour;
  Tetrahedron * _rightNeighbour;
};

class Triangle6Face : public Triangle6 {
public:
  Triangle6Face( Node & n1, Node & n2, Node & n3, int id = -1, int marker = -1 );
  virtual ~Triangle6Face();
  virtual int rtti() const { return MYMESH_TRIANGLE6FACE_RTTI; };

  virtual double jacobianDeterminant() const;
  virtual double area() const;
  virtual RealPos normVector() const ;

protected:
};

//!A Quadrangle;
/*!A Quadrangle, with an id, four nodes and one vector of attributes */
class Quadrangle : public BaseElement{
public:
  //** bis jetzt nur  Paralelogramm ;
  Quadrangle(){ cerr << __ASSERT_FUNCTION << " should not be used." << endl; }
  Quadrangle( Node & n1, Node & n2, Node & n3, Node & n4, int id = -1, double attribute = 1 );
  Quadrangle( Quadrangle & quad );
  virtual ~Quadrangle();
  virtual int rtti() const { return MYMESH_QUADRANGLE_RTTI; };
  Quadrangle & operator = ( Quadrangle & quad );
  Edge & edge( int i ) const { return * _edges[ i ]; }

  //  virtual double jacobianDeterminant( Node * n0 = NULL, Node * n1 = NULL, Node * n2 = NULL ); // Wegen Paralleogramm moeglich
  virtual int shapeNodeCount() const { return 4; }
  virtual double jacobianDeterminant( ) const;
  virtual double area() const { return jacobianDeterminant(); }

  virtual double partDerivationRealToUnity( char absPos, int relPos );
  RealPos middlePosition();
  /*! Sets the References nodes to Nodes 1 upto 4. */
  virtual void setNodes(Node & n1, Node & n2, Node & n3, Node & n4);
//   virtual void setNode(int i, Node & node){ assert( i > -1 && i < 5 ); _nodesVector[i] = & node; }
//   virtual Node & node( int i ) { assert( i > -1 && i < 5 ); return *_nodesVector[i]; }
  VectorpEdges collectEdges(Node & n1, Node & n2, Node & n3, Node & n4);
  //  int nodeCount(){ return _nodesVector.size(); }
protected:
  Edge * _edges[4];// Q&D nearly obsolete

};

class QuadrangleFace : public Quadrangle {
public:
  QuadrangleFace( Node & n1, Node & n2, Node & n3, Node & n4, int id = -1, int marker = -1 );
  virtual int rtti() const { return MYMESH_QUADRANGLEFACE_RTTI; };
protected:
  Hexahedron * leftNeighbour_;
  Hexahedron * rightNeighbour_;
};

class Tetrahedron : public BaseElement{
public:
  Tetrahedron(){ DO_NOT_USE }
  Tetrahedron( Node & n1, Node & n2, Node & n3, Node & n4, int id = -1, double attribute = 1.0 );

  virtual int rtti() const { return MYMESH_TETRAHEDRON_RTTI; };
  //  Tetrahedron( const Tetrahedron & tet ) : BaseElement() { TO_IMPL }
  virtual ~Tetrahedron(){ _nodesVector.clear(); }

  //  Tetrahedron & operator = ( Tetrahedron & tet ){ TO_IMPL }

  virtual int shapeNodeCount() const { return 4; }
//   RealPos middlePosition();
  virtual void setNodes(Node & n1, Node & n2, Node & n3, Node & n4){
    _nodesVector[0] = & n1;
    _nodesVector[1] = & n2;
    _nodesVector[2] = & n3;
    _nodesVector[3] = & n4;
  }

  virtual double area() const { return volume(); }
  virtual double partDerivationRealToUnity( char absPos, int relPos );
  virtual MyVec::STLMatrix jacobianMat( ) const;
  virtual double jacobianDeterminant( ) const;

  RealPos middlePosition();
  virtual bool touch( const RealPos & pos, double tol, bool verbose = false ) const;

  void scale( double scale );
  void shrink( double shrink );
  void moveTo( RealPos & newpos );

//   virtual void setNode(int i, Node & node);
//   virtual Node & node( int i );

  double volume() const { return fabs( jacobianDeterminant() / 6. ); }

  virtual void switchDirection();

  TriangleFace * face( size_t i ){ return _face[ i ]; }
  void setFace( size_t i, TriangleFace * face ){ _face[ i ] = face; }

protected:
  TriangleFace * _face[ 4 ];
};

class Tetrahedron10 : public Tetrahedron{
public:
  Tetrahedron10( Node & n1, Node & n2, Node & n3, Node & n4, int id = -1, double attribute = 1.0 );
  virtual ~Tetrahedron10();

  virtual int rtti() const { return MYMESH_TETRAHEDRON10_RTTI; }
  void check();
  void switchDirection();
protected:
  int checkEdge( int a, int b, int c);
};

class Hexahedron : public BaseElement{
public:
  Hexahedron( Node & n1, Node & n2, Node & n3, Node & n4, Node & n5, Node & n6, Node & n7, Node & n8, int id = -1, double attribute = 1.0 );
  virtual int rtti() const { return MYMESH_HEXAHEDRON_RTTI; }

  virtual int shapeNodeCount() const { return 8; }
  virtual double jacobianDeterminant( ) const { TO_IMPL; return 0.0; }
  virtual double area() const { TO_IMPL; return 0.0; }
protected:
};

DLLEXPORT int operator == ( const Edge & e1, const Edge & e2 );
DLLEXPORT int operator != ( const Edge & e1, const Edge & e2 );
DLLEXPORT int operator == ( const RealPos & a , const RealPos & b );
DLLEXPORT int operator != ( const RealPos & a , const RealPos & b );
DLLEXPORT int operator == ( const IntPos & a , const IntPos & b );
DLLEXPORT int operator != ( const IntPos & a , const IntPos & b );

} // namespace MyMesh
#endif // ELEMENTS__H

/*
$Log: elements.h,v $
Revision 1.40  2008/10/14 12:24:46  carsten
*** empty log message ***

Revision 1.39  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.38  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.37  2008/03/14 11:16:25  carsten
*** empty log message ***

Revision 1.36  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.35  2007/04/04 18:35:04  carsten
*** empty log message ***

Revision 1.34  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.33  2006/10/06 16:53:39  carsten
*** empty log message ***

Revision 1.32  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.31  2006/01/29 17:54:05  carsten
*** empty log message ***

Revision 1.30  2005/12/06 13:57:26  carsten
*** empty log message ***


Revision 1.27  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.26  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.25  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.24  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.23  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.22  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.21  2005/10/04 11:07:42  carsten
*** empty log message ***

Revision 1.20  2005/09/08 19:08:48  carsten
*** empty log message ***

Revision 1.19  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.18  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.17  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.16  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.15  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.14  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.13  2005/05/18 14:36:13  carsten
*** empty log message ***

Revision 1.12  2005/05/03 18:46:46  carsten
*** empty log message ***

Revision 1.11  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.10  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.9  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.8  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.7  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.6  2005/01/12 21:06:45  carsten
*** empty log message ***

Revision 1.5  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.4  2005/01/06 19:10:52  carsten
*** empty log message ***

Revision 1.3  2005/01/06 17:07:42  carsten
*** empty log message ***

Revision 1.2  2005/01/05 13:29:16  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

 */
