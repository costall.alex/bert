Example 2a (plate electrodes) from R�cker&G�nther (2011)

See paper for details. 

References:
R�cker, C. & G�nther, T. (2011) The simulation of finite ERT electrodes using the complete electrode model. Geophysics, 76, F227.