
import pygimli as pg
import numpy as np

# dimensions (length,width,height) of the tank
lenT, widT, heiT = 0.60325, 0.29845, 0.2159
xel0 = 0.1651  # distance from xmin to first electrode

poly = pg.Mesh(2)
n0 = poly.createNode(-xel0, 0.0, 0.)  # four corners
n1 = poly.createNode(lenT-xel0, 0.0, 0.)  # so that first el. is at 0
n2 = poly.createNode(lenT-xel0, -heiT, 0.)
n3 = poly.createNode(0.3, -heiT, 0.)
n4 = poly.createNode(0.0, -heiT, 0.)
n5 = poly.createNode(-xel0, -heiT, 0.)

# create rectangle out of the six points with Neumann BC marker
poly.createEdge(n0, n1, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(n1, n2, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(n2, n3, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(n3, n4, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(n4, n5, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
poly.createEdge(n5, n0, pg.MARKER_BOUND_HOMOGEN_NEUMANN)
#    0--------------1
#    |              |
#    5---4------3---2

# add electrodes as nodes in the mesh
data = pg.DataContainer('tankdata.ohm')
del0 = data.sensorPosition(1).x() - data.sensorPosition(0).x()
for posx in pg.x(data.sensorPositions()):
    ne = poly.createNode(posx, 0., 0., 0)  # not yet electrodes
    ne = poly.createNode(posx + del0/2, 0., 0., 0)  # midpoint

mesh2 = pg.Mesh(2)  # Poly,zero,.edge,Attr,area,quality,Quiet
pg.TriangleWrapper(poly, mesh2, '-pzeAfa0.002q34.8')
print(mesh2)
mesh2.save('showMesh.bms')  # save to display result in 2d (pytripatch)

# enumerate in ascending order to make them individual regions
for i, c in enumerate(mesh2.cells()):
    c.setMarker(i)

# create z vector and extrude 2d mesh into z direction yielding 3d mesh
dz = data.sensorPosition(1)[0] - data.sensorPosition(0)[0]
nz = np.fix(widT/dz/2.) * 2 + 1  # ensure z=0 in the middle (odd number)
z = np.linspace(-widT/2., widT/2., nz)
mesh3 = pg.createMesh3D(mesh2, z, pg.MARKER_BOUND_HOMOGEN_NEUMANN,
                        pg.MARKER_BOUND_HOMOGEN_NEUMANN)
# exchange y and z
for nn in mesh3.nodes():
    pos = nn.pos()
    nn.setPos(pg.RVector3(pos.x(), pos.z(), pos.y()))

# add current and voltage calibration points where they are in prim mesh
mesh3.createNodeWithCheck(pg.RVector3(0.0, 0., -heiT)).setMarker(-999)
mesh3.createNodeWithCheck(pg.RVector3(0.3, 0., -heiT)).setMarker(-1000)

# add electrode nodes at positions where there are already nodes
print mesh3
for pos in data.sensorPositions():
    mesh3.createNodeWithCheck(pos).setMarker(-99)

print mesh3
mesh3.exportVTK('prisMesh.vtk')
mesh3.save('mesh/mesh.bms')
