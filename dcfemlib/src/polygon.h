// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef POLYGON__H
#define POLYGON__H POLYGON__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
using namespace MyMesh;

#include <deque>
using namespace std;

class DLLEXPORT Polygon : public deque< Node * > {
public:
  Polygon( int marker = 0 ) : marker_( marker ) { }
  Polygon( Node * n1, Node * n2, int marker = 0 );
  Polygon( const Polygon & poly );
  void switchDirection();
  void transform( double mat[ 4 ][ 4 ] );
  void add( const Polygon & b );
  void insertNode( Node * node, size_t position );

  /*! Returns true if this Polygon already contains the Node, else it returns false.*/
  bool contains( Node * node );

  /*! Returns the index where pos touches the polygon, otherwise returns -1. */
  int touch( const RealPos & pos, double tol = TOLERANCE );

  int marker() const { return marker_; }
  void setMarker( int marker ) { marker_ = marker; }

protected:
  int marker_;
};

ostream & operator << ( ostream & str, const Polygon & poly );

#endif // POLYGON__H
/*
$Log: polygon.h,v $
Revision 1.5  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.4  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.3  2005/10/20 16:54:55  carsten
*** empty log message ***

Revision 1.2  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
