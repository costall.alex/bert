bert->Examples->Inversion->3dTopo->slagdump
###############################################

This example is the field data set from a slagdump presented by G�nther et al. (2006), the original BERT paper.
It comprises 13 parallel profiles with a rather large profile distance plus a perpendicular one using Wenner array.

G�nther, T., R�cker, C., and Spitzer, K. (2006). 3-d modeling and inversion of DC resistivity
data incorporating topography - Part II: Inversion. Geophys. J. Int., 166(2):506-517.