// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
using namespace DCFEMLib;

#include <longoptions.h>
#include <mesh2d.h>
#include <mesh3d.h>
using namespace MyMesh;

using namespace std;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("mesh");
  lOpt.setDescription( (string) "Create secondary mesh from parameter mesh: parameshPara.x parameshSec.x " +
		       "mesh needs the cell-attributes 1 for boundary domain and 2 for parameter domain." +
		       "medit-mesh will also create" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "refineIter", no_argument, 'r', "Refinement iterations [1]" );
  lOpt.insert( "p2refine", no_argument, 'p' , "Generate p2 refined mesh [false]" );
  lOpt.insert( "Dimension", required_argument, 'd', "Dimension [ 3 ]" );

  int option_char = 0, option_index = 0;
  bool verbose = false, refinep2 = false;
  int dimension = 3, refineIterations = 1;
  while ( ( option_char = getopt_long( argc, argv, "?hv"
				       "O" // Override
				       "r:" // refine
				       "p" // generate p2
				       "d:", // dimension,
				       lOpt.long_options(), & option_index ) ) != EOF ){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[ 0 ] ); return 1; break;
    case 'h': lOpt.printHelp( argv[ 0 ] ); return 1; break;
    case 'd': dimension = atoi( optarg ); break;
    case 'r': refineIterations = atoi( optarg ); break;
    case 'p': refinep2 = true; break;
    case 'v': verbose = true; break;
    default : cerr << WHERE_AM_I << " undefined option: " << option_char << endl;
    }
  }
  string basenameInput = NOT_DEFINED;

  if ( argc < 2 ){
    lOpt.printHelp( argv[ 0 ] );
    return 0;
  } else {
    basenameInput = argv[ argc - 1 ];
  }

  string basename( basenameInput.substr( 0, basenameInput.rfind( MESHBINSUFFIX ) ) );

  BaseMesh *mesh = NULL, *para = NULL, *sec = NULL;
  BaseMesh *secTmp = NULL;
  //  BaseMesh *paraRefined = NULL;

  switch ( dimension ){
  case 2:
    mesh = new Mesh2D();
    para = new Mesh2D();
    sec = new Mesh2D();
    secTmp = new Mesh2D();
    //    paraRefined = new Mesh2D();
    if ( verbose ) cout << "Load basemesh ... " << endl;
    mesh->load( basenameInput + MESHBINSUFFIX);
    break;
  case 3:
    mesh = new Mesh3D();
    para = new Mesh3D();
    sec = new Mesh3D();
    secTmp = new Mesh3D();
    //    paraRefined = new Mesh3D();
    if ( verbose ) cout << "Load basemesh ... " << endl;
    if ( basenameInput.find( ".bms" ) != string::npos ) {
      mesh->load( basenameInput );}
    else{
      dynamic_cast< Mesh3D*>( mesh )->loadTetGenMesh( basenameInput );
    }
    break;
  }
  if ( verbose ) mesh->showInfos();


  //** parameter recount;
  int count = 2;
  for ( int i = 0; i < mesh->cellCount(); i ++ ){
    if ( mesh->cell( i ).attribute() == 2 ){
      mesh->cell( i ).setAttribute( count );
      count++;
    }
  }

  if  ( verbose ) cout << "Found " << count - 2 << " parameters." << endl;

  *sec = *mesh ;

//   if ( sec->dim() == 2 ){
//     vector < int > cellToRefine;
//     for ( int i = 0; i < sec->cellCount(); i ++ ){
//       if ( sec->cell( i ).node( 0 ).findEdge( sec->cell( i ).node( 1 ) )->marker() == -1 ){
// 	cellToRefine.push_back( i ); continue;
//       }
//       if ( sec->cell( i ).node( 1 ).findEdge( sec->cell( i ).node( 2 ) )->marker() == -1 ){
// 	cellToRefine.push_back( i ); continue;
//       }
//       if ( sec->cell( i ).node( 2 ).findEdge( sec->cell( i ).node( 0 ) )->marker() == -1 ){
// 	cellToRefine.push_back( i ); continue;
//       }
//     }
//     secTmp->createRefined( *sec, cellToRefine );
//     *sec = *secTmp ;
//   }

  for ( int i = 0; i < refineIterations; i ++ ){
    secTmp->createH2Mesh( *sec );
    *sec = *secTmp ;
    if ( verbose ) sec->showInfos();
  }
  if ( refinep2 ){
    secTmp->createP2Mesh( *sec );
    *sec = *secTmp ;
    if ( verbose ) sec->showInfos();
  }

  //  sec->improveMeshQuality( 1, 0, 1, 4 );

  para->findSubMeshByAttribute( *mesh, (double)2, (double)count );
  if ( verbose ) para->showInfos();
  //  paraRefined->findSubMeshByAttribute( *sec, (double)2, (double)count );

  if ( verbose ) cout << "save meshs ... " << endl;

  switch ( dimension ){
  case 2:
    break;
  case 3:
    dynamic_cast< Mesh3D * >(mesh)->saveINRIAMeshFile( basename + "Para" );
    dynamic_cast< Mesh3D * >(sec)->saveINRIAMeshFile( basename + "Sec" );
    dynamic_cast< Mesh3D * >(para)->saveINRIAMeshFile( basename + "ParaDomain" );
    dynamic_cast< Mesh3D * >(para)->saveVTK( basename + "ParaDomain" );
    //    dynamic_cast< Mesh3D * >(paraRefined)->saveINRIAMeshFile( basename + "SecParaDomain" );
    break;
  }
  mesh->save( basename + "Para" + MESHBINSUFFIX );
  sec->save( basename + "Sec" + MESHBINSUFFIX );
  para->save( basename + "ParaDomain" + MESHBINSUFFIX );
  //  paraRefined->save( basename + "SecParaDomain" );

  return 0;
}

/*
$Log: createSecondaryMesh.cpp,v $
Revision 1.19  2009/07/09 22:31:04  carsten
some small fixes and simple 1.check-script for examples

Revision 1.18  2009/01/30 16:54:26  carsten
*** empty log message ***

Revision 1.17  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.16  2008/03/01 23:00:48  carsten
*** empty log message ***

Revision 1.15  2007/05/22 18:50:04  carsten
*** empty log message ***

Revision 1.14  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.13  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.12  2006/08/27 18:58:03  carsten
*** empty log message ***

Revision 1.11  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.10  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.9  2005/09/28 13:07:23  carsten
*** empty log message ***

Revision 1.8  2005/09/26 17:23:59  carsten
*** empty log message ***

Revision 1.7  2005/08/30 17:40:05  carsten
*** empty log message ***

Revision 1.6  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.5  2005/06/02 10:50:45  carsten
*** empty log message ***

Revision 1.4  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.3  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.2  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.1  2005/03/29 16:57:46  carsten
add inversion tools

*/
